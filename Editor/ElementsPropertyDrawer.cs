﻿using System;
using UnityEditor;
using UnityEngine;

namespace EditorUtils.ResOptimization
{
    [CustomPropertyDrawer(typeof(ResourcesConfig.NameStyle))]
    public class ElementsPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.BeginChangeCheck();

            property.enumValueIndex = EditorGUI.Popup(position, label, property.enumValueIndex, GetNames());
            
            if (EditorGUI.EndChangeCheck())
            {
                property.serializedObject.ApplyModifiedProperties();
            }
            EditorGUI.EndProperty();
        }

        private GUIContent[] GetNames()
        {
            if (s_enumNames == null)
            {
               var names = Enum.GetNames(typeof(ResourcesConfig.NameStyle));
               s_enumNames = new GUIContent[names.Length];
               for (int i = 0; i < names.Length; i++)
               {
                   string enumValueName = names[i];
                   s_enumNames[i] = new GUIContent(enumValueName);
               }
            }

            return s_enumNames;
        }
        
        private static GUIContent[] s_enumNames = null;
    }
}
