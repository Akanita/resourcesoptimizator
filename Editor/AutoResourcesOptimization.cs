﻿using UnityEditor;

namespace EditorUtils.ResOptimization
{
    public class AutoResourcesOptimization : AssetPostprocessor
    {
        private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets,
            string[] movedAssets,
            string[] movedFromAssetPaths)
        {
            //Debug.Log("OnPostprocessAllAssets: " + string.Join(", ", importedAssets));
            foreach (string assetPath in importedAssets)
            {
                ResourcesConfig.FindAndReimport(assetPath);
            }
        }
    }
}