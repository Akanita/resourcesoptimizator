﻿using System.IO;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace EditorUtils.ResOptimization
{
    public static class CreateFbxPrefab
    {
        // [MenuItem("Assets/Utils/Create Prefabs")]
        // private static void CreatePrefabsFromModels()
        // {
        //     var targetObjects = Selection.objects;
        //     foreach (var obj in targetObjects)
        //     {
        //         if (obj == null) continue;
        //         if (!(obj is GameObject gObj)) continue;
        //
        //         var type = PrefabUtility.GetPrefabAssetType(gObj);
        //         if (type != PrefabAssetType.Model) continue;
        //         CreateModelPrefab(AssetDatabase.GetAssetPath(gObj));
        //         ResourcesConfig.FindAndReimport(AssetDatabase.GetAssetPath(gObj));
        //     }
        // }

        #region FixModelPrefab

        // private static void CreateModelPrefab(string assetPath)
        // {
        //     //TODO rework
        //     GameObject model = (GameObject) AssetDatabase.LoadMainAssetAtPath(assetPath);
        //     if (model == null)
        //         return;
        //
        //     string pathFrom = assetPath.Replace(OptimizatorConfig.Instance.FbxPath, "");
        //     pathFrom = pathFrom.Replace(Path.GetFileName(assetPath), "");
        //
        //     string directory = OptimizatorConfig.Instance.PrefabPath + "Models/" + pathFrom;
        //     string path = directory + model.name + ".prefab";
        //
        //     CreateDirection.CreateDirIfNotExists(directory);
        //
        //     GameObject objScene = Object.Instantiate(model);
        //     ResourcesConfig.FindAndReimport(AssetDatabase.GetAssetPath(objScene));
        //
        //     //Debug.Log("Create prefab");
        //     //SetCorrectName.RenameFile(objScene);
        //
        //     PrefabUtility.SaveAsPrefabAsset(objScene, path);
        //
        //     Object.DestroyImmediate(objScene);
        //     AssetDatabase.SaveAssets();
        // }

        #endregion

        #region FixTextxureQualittySetting

        private static void TrySetTextureSettings(TextureImporter importer, TextureImporterPlatformSettings tips)
        {
            var curSettings = importer.GetPlatformTextureSettings(tips.name);
            bool isSame =
                curSettings.name == tips.name &&
                curSettings.overridden == tips.overridden &&
                curSettings.maxTextureSize == tips.maxTextureSize &&
                curSettings.compressionQuality == tips.compressionQuality &&
                curSettings.format == tips.format;
            if (isSame)
            {
                return;
            }

            importer.SetPlatformTextureSettings(tips);
        }

        public static void FixTexture(TextureImporter importer)
        {
            bool isHaveAlphaCannel = importer.DoesSourceTextureHaveAlpha();
            importer.mipmapEnabled = false;
            importer.isReadable = false;
            importer.alphaSource =
                isHaveAlphaCannel ? TextureImporterAlphaSource.FromInput : TextureImporterAlphaSource.None;
            importer.alphaIsTransparency = importer.DoesSourceTextureHaveAlpha();

            // Texture compression setting
            TrySetTextureSettings(importer,
                new TextureImporterPlatformSettings()
                {
                    name = "Standalone",
                    overridden = true,
                    maxTextureSize = 2048,
                    compressionQuality = 100,
                    format =
                        isHaveAlphaCannel ? TextureImporterFormat.DXT5Crunched : TextureImporterFormat.DXT1Crunched,
                });

            TrySetTextureSettings(importer,
                new TextureImporterPlatformSettings()
                {
                    name = "iPhone",
                    overridden = true,
                    maxTextureSize = 2048,
                    compressionQuality = 50,
                    format = isHaveAlphaCannel ? TextureImporterFormat.ASTC_5x5 : TextureImporterFormat.PVRTC_RGB4,
                });

            TrySetTextureSettings(importer,
                new TextureImporterPlatformSettings()
                {
                    name = "Android",
                    overridden = true,
                    maxTextureSize = 2048,
                    compressionQuality = 50,
                    format = isHaveAlphaCannel
                        ? TextureImporterFormat.ETC2_RGBA8Crunched
                        : TextureImporterFormat.ETC2_RGB4
                });

            if (importer.textureType == TextureImporterType.Sprite)
            {
                TextureImporterSettings textureSettings = new TextureImporterSettings();
                importer.ReadTextureSettings(textureSettings);
                textureSettings.spriteMeshType = SpriteMeshType.FullRect;
                textureSettings.spriteGenerateFallbackPhysicsShape = false;
                importer.SetTextureSettings(textureSettings);
            }

            if (EditorUtility.IsDirty(importer.GetInstanceID()))
            {
                importer.SaveAndReimport();
            }
        }

        #endregion
    }
}