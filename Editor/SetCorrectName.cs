﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.IO;
using System.Linq;
using Object = UnityEngine.Object;

namespace EditorUtils.ResOptimization
{
    public static class SetCorrectName
    {
	    private static Dictionary<ResourcesConfig.NameStyle, Dictionary<string, string>>
		    s_nameStyleNames = new Dictionary<ResourcesConfig.NameStyle, Dictionary<string, string>>();
		
        private static string GetCorrectName(ResourcesConfig.NameStyle nameStyle, string path,
                bool onlyName = true) //Function about get new correct names
        {
	        if (!s_nameStyleNames.TryGetValue(nameStyle, out var namesMap))
	        {
		        namesMap = new Dictionary<string, string>();
		        s_nameStyleNames[nameStyle] = namesMap;
	        }
            char[] separators = {'_', ' ', '(', ')', '[', ']', '.', ',', '-'};
            string partName = onlyName ? Path.GetFileNameWithoutExtension(path) : path;

            string[] partSlash = partName.Split('/');
			
            for (int y = 0; y < partSlash.Length; y++)
            {
                string part = partSlash[y];
                if (namesMap.TryGetValue(part, out var res))
                {
	                partSlash[y] = res;
					continue;
                }
                var partOfName = part.Split(separators, StringSplitOptions.RemoveEmptyEntries).ToList();

                for (int index = 0; index < partOfName.Count; index++)
                {
                    string str = partOfName[index];
                     //Debug.Log($"{index}. str='{str}': {path}");
                    bool prevIsNumber = char.IsNumber(str[0]);
                    for (int i = 1; i < str.Length; i++)
                    {
                        bool needSplit = false;
                        needSplit |= char.IsUpper(str[i]);
                        needSplit |= prevIsNumber && !char.IsNumber(str[i]);
                        prevIsNumber = char.IsNumber(str[i]);
                        if (!needSplit)
                        {
                            continue;
                        }

                        partOfName[index] = str.Substring(0, i);
                        string secondWord = str.Substring(i);
                        partOfName.Insert(index + 1, secondWord);
                        break;
                    }
                }

                for (int index = 0; index < partOfName.Count; index++)
                {
                    char[] newPt = partOfName[index].ToLower().ToCharArray();
                    switch (nameStyle)
                    {
                        case ResourcesConfig.NameStyle.camelCase:
                            if (index != 0) newPt[0] = char.ToUpper(newPt[0]);
                            break;
                        case ResourcesConfig.NameStyle.PascalCase:
                            newPt[0] = char.ToUpper(newPt[0]);
                            break;
                        case ResourcesConfig.NameStyle.snake_case:
                            if (index != 0) newPt = ("_" + new string(newPt)).ToCharArray();
                            break;
                    }

                    partOfName[index] = new string(newPt);
                }

				string resPart = string.Join(string.Empty, partOfName);
				partSlash[y] = resPart;
				namesMap[part] = resPart;
				namesMap[resPart] = resPart;
            }

            partName = string.Join("/", partSlash);
            return partName;
        }
        
        private static void SetCorrectNameObj(Object forObject, string correctName)
        {
            if (AssetDatabase.IsMainAsset(forObject))
            {
                bool needGenerateName = true;
                string path = AssetDatabase.GetAssetOrScenePath(forObject);
                while (needGenerateName)
                {
                    string targetPath = path;
                    string fileExt = Path.GetExtension(targetPath);
                    string fileName = Path.GetFileName(targetPath);
                    if (targetPath != null)
                    {
                        string dir = targetPath.Substring(0, targetPath.Length - fileName.Length);
                        string targetPathNewName = Path.Combine(dir, correctName + fileExt);
                        string uniqName = AssetDatabase.GenerateUniqueAssetPath(targetPathNewName);
                        string doubleCorrectedName = Path.GetFileNameWithoutExtension(uniqName);
						 //Debug.Log("dir=" + dir + "; "+ targetPathNewName + "\r\n" + "uniq=" + uniqName + "; double=" + doubleCorrectedName);
                        needGenerateName = correctName != doubleCorrectedName;
                        correctName = doubleCorrectedName;
                    }
                }

                 //Debug.Log("Rename " + path + " => " + correctName);
                string error = AssetDatabase.RenameAsset(path, correctName);
                if (!string.IsNullOrEmpty(error)) Debug.LogError(error);
            }

            forObject.name = correctName;
             //Debug.Log("Set dirty for " + forObject, forObject);
            EditorUtility.SetDirty(forObject);
        }
        
        private static bool ChangeNamesForObjects(ResourcesConfig.NameStyle nameStyle, Object obj) //Change names for Objects
        {
            string correctName = GetCorrectName(nameStyle, obj.name);
            if (obj.name == correctName)
            {
	            return false;
            }
            SetCorrectNameObj(obj, correctName);
             //Debug.Log("SaveAssets " + obj, obj);
            AssetDatabase.SaveAssets();
            return true;
        }

        #region ChangeNameForGameObject

        private static void ChangeNameForGameObject(ResourcesConfig.NameStyle nameStyle, GameObject gameObj) //Change names for Game Object
        {
#if UNITY_2018_3_OR_NEWER
            var type = PrefabUtility.GetPrefabAssetType(gameObj);
            if (type == PrefabAssetType.Model || type == PrefabAssetType.Variant)
                return;
            RenameRootPrefab(nameStyle, gameObj);
#endif
            //FindAnimationsInGameObject(gameObj);
        }

        private static bool RenameRootPrefab(ResourcesConfig.NameStyle nameStyle, GameObject gameObject) //Change names for Root Game Object
        {
			 //Debug.Log("RenameRootPrefab " + gameObject + ": " + gameObject.GetHashCode());
            var path = PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(gameObject);
            var newName = GetCorrectName(nameStyle, gameObject.name);
			bool isNameChanged = gameObject.name != newName;
			isNameChanged |= ChangeNameForNestedPrefab(nameStyle, gameObject, gameObject);
            if (!isNameChanged)
            {
	            return false;
            }
			
            gameObject.name = newName;
             //Debug.Log("RenameRootPrefab " + path + " => " + newName, gameObject);
            PrefabUtility.SavePrefabAsset(gameObject);
            AssetDatabase.RenameAsset(path, newName + ".prefab");
            return false;
        }


#if UNITY_2018_3_OR_NEWER
        private static bool ChangeNameForNestedPrefab(ResourcesConfig.NameStyle nameStyle, GameObject gameObject,
                GameObject rootPrefab = null) //Change names for Nested Prefabs
        {
	        bool isAnyChanged = false;
            foreach (Transform child in gameObject.transform)
            {
                var childObj = child.gameObject;
                var isRoot = PrefabUtility.IsOutermostPrefabInstanceRoot(childObj);
                if (childObj.name.Contains("Missing Prefab"))
                {
	                Debug.LogError($"{rootPrefab} has missing prefab {childObj.name}");
	                continue;
                }
                if (PrefabUtility.IsPrefabAssetMissing(childObj))
                {
	                Debug.LogError($"{rootPrefab} has missing prefab {childObj.name}");
	                continue;
                }
                if (PrefabUtility.IsDisconnectedFromPrefabAsset(childObj))
                {
	                Debug.LogError($"{rootPrefab} has missing prefab {childObj.name}");
	                continue;
                }
                if (!isRoot)
                {
                    if (PrefabUtility.IsAddedGameObjectOverride(childObj) ||
                        PrefabUtility.GetNearestPrefabInstanceRoot(childObj) == rootPrefab ||
                        PrefabUtility.GetNearestPrefabInstanceRoot(childObj) == null)
                    {
	                    isAnyChanged |= ChangeNamesForObjects(nameStyle, childObj);
                    }
                    else if (PrefabUtility.IsAnyPrefabInstanceRoot(childObj))
                    {
	                    isAnyChanged |= ResetNameOverride(childObj);
                    }

                    isAnyChanged |= ChangeNameForNestedPrefab(nameStyle, childObj);
                    continue;
                }

                var childPath = PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(childObj);
                var childPrefab = AssetDatabase.LoadAssetAtPath(childPath, typeof(GameObject));
                isAnyChanged |= RenameRootPrefab(nameStyle, childPrefab as GameObject);
                isAnyChanged |= ResetNameOverride(childObj);
                isAnyChanged |= ChangeNameForNestedPrefab(nameStyle, childObj, rootPrefab);
            }
			
            if (isAnyChanged)
            {
	             //Debug.Log("SetDirty in " + gameObject, gameObject);
	            EditorUtility.SetDirty(gameObject);
            }
            return isAnyChanged;
        }

        private static bool ResetNameOverride(GameObject gameObj)
        {
            var childSerialize = new SerializedObject(gameObj);
            var nameProp = childSerialize.FindProperty("m_Name");
            string oldVal = nameProp.stringValue;

            PrefabUtility.RevertPropertyOverride(nameProp, InteractionMode.AutomatedAction);
			
            childSerialize = new SerializedObject(gameObj);
            nameProp = childSerialize.FindProperty("m_Name");
            string newVal = nameProp.stringValue;
            return oldVal != newVal;
        }

#endif

        #endregion
        //
        // #region ChangeNameInAnimation
        //
        // private static void FindAnimationsInGameObject(GameObject gameObj)
        // {
        //     var animComps = gameObj.GetComponentsInChildren<Animation>();
        //     if (animComps.Length == 0)
        //         return;
        //     foreach (var animation in animComps)
        //     {
        //         foreach (AnimationState clip in animation)
        //         {
        //             ChangeNamesForAnimationKeys(clip.clip);
        //         }
        //     }
        //
        //     Debug.Log("Change name " + gameObj.name, gameObj);
        //     EditorUtility.SetDirty(gameObj);
        //     PrefabUtility.SavePrefabAsset(gameObj);
        // }
        //
        // private static void ChangeNamesForAnimationKeys(AnimationClip anim) //Change names for AnimationsClip
        // {
        //     var path = AssetDatabase.GetAssetPath(anim);
        //     anim = AssetDatabase.LoadMainAssetAtPath(path) as AnimationClip;
        //     EditorCurveBinding[] bindings = AnimationUtility.GetCurveBindings(anim);
        //     for (int i = 0; i < bindings.Length; i++)
        //     {
        //         var curve = AnimationUtility.GetEditorCurve(anim, bindings[i]);
        //         AnimationUtility.SetEditorCurve(anim, bindings[i], null);
        //         string correctPath = GetCorrectName(bindings[i].path, false);
        //         bindings[i].path = correctPath;
        //         AnimationUtility.SetEditorCurve(anim, bindings[i], curve);
        //     }
        //
        //     AssetDatabase.SaveAssets();
        // }
        //
        // #endregion

        public static string RenameFile(ResourcesConfig.NameStyle nameStyle, Object obj)
        {
            var path = AssetDatabase.GetAssetPath(obj);
            var newFileName = Path.GetFileNameWithoutExtension(path);
            var gameObj = obj as GameObject;
            if (gameObj == null)
            {
                ChangeNamesForObjects(nameStyle, obj);
                newFileName = obj.name;
            }
            else
            {
	            Object prefab = null;
	            try
	            {
		            AssetDatabase.StartAssetEditing();
		            prefab = AssetDatabase.LoadAssetAtPath(path, typeof(GameObject));
		            ChangeNameForGameObject(nameStyle, prefab as GameObject);
	            }
	            catch (Exception e)
	            {
					Debug.LogError(e);
	            }
	            finally
	            {
		            AssetDatabase.StopAssetEditing();
		            // TODO:
		            if (prefab != null)
		            {
			            newFileName = prefab.name;
		            }
	            }
            }

            var dir = Path.GetDirectoryName(path);
            var newFullFileName = newFileName + Path.GetExtension(path);
            return Path.Combine(dir, newFullFileName);
        }
    }
}