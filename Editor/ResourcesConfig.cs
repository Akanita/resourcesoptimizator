﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEditor.Presets;
using UnityEngine;
using Object = UnityEngine.Object;

namespace EditorUtils.ResOptimization
{
    [CreateAssetMenu]
    public class ResourcesConfig : ScriptableObject
    {
        [Serializable]
        public class ResourcesInfo
        {
            [SerializeField] private string m_path = String.Empty;
            [SerializeField] private Preset m_preset = null;
            [SerializeField] private NameStyle m_nameStyle = NameStyle.camelCase;

            public string Path => m_path;
            public Preset Preset => m_preset;
            public NameStyle NameStyle => m_nameStyle;
        }

        public enum NameStyle
        {
            camelCase,
            PascalCase,
            snake_case
        }

        [SerializeField] private List<ResourcesInfo> m_list = new List<ResourcesInfo>();

        private static List<ResourcesConfig> m_resConfigs = new List<ResourcesConfig>();
        private static Dictionary<string, Type> m_allComponentNames = new Dictionary<string, Type>();
        private static int s_changesCounter = 0;

        private List<ResourcesInfo> ListConfigs => m_list;
        
        private static Type GetTypeByName(string typeName)
        {
            if (m_allComponentNames.Count == 0)
            {
                var allTypes = AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.GetTypes());
                var componentTypes = allTypes.Where(t => typeof(Component).IsAssignableFrom(t));
                foreach (Type componentType in componentTypes)
                {
                    m_allComponentNames[componentType.FullName] = componentType;
                    var customNativeNameAttr = componentType.GetCustomAttributes()
                        .FirstOrDefault(ca => ca.GetType().Name == "NativeClassAttribute");
                    if (customNativeNameAttr == null) continue;
                    var prop = customNativeNameAttr.GetType().GetProperty("QualifiedNativeName");
                    var val = prop.GetValue(customNativeNameAttr);
                    // Can be null for some types of unity
                    if (val == null) continue;
                    // Convert C++ to C# style
                    var resultName = ((string) val).Replace("::", ".");
                    m_allComponentNames.Add(resultName, componentType);
                }
            }

            return m_allComponentNames.TryGetValue(typeName, out Type type) ? type : null;
        }
        
        private static void FindResConfigs()
        {
            m_resConfigs.Clear();
            var configsPath = AssetDatabase.FindAssets("t:" + typeof(ResourcesConfig).Name).ToList();

            if (configsPath.Any(cp => cp.StartsWith("Assets")))
            {
                // have not default
                configsPath.RemoveAll(c => c.StartsWith("Packages"));
            }

            foreach (string configPath in configsPath)
            {
                string path = AssetDatabase.GUIDToAssetPath(configPath);
                var config = AssetDatabase.LoadAssetAtPath<ResourcesConfig>(path);
                if (ValidatCanBeAdded(m_resConfigs, config))
                {
	                m_resConfigs.Add(config);
                }
                else
                {
	                Debug.LogError($"Can`t use {config}, see errors above for more info", config);
                }
            }
        }

        private static bool ValidatCanBeAdded(List<ResourcesConfig> resConfigs, ResourcesConfig config)
        {
	        bool res = true;
	        for (var i = 0; i < config.ListConfigs.Count; i++)
	        {
		        res &= CheckLists(config, i, config, i + 1);
	        }

	        for (var i = 0; i < config.ListConfigs.Count; i++)
	        {
		        foreach (var resourcesConfig in resConfigs)
		        {
			        res &= CheckLists(config, i, resourcesConfig, 0);
		        }
	        }

	        return res;
        }

        private static bool CheckLists(ResourcesConfig config, 
	        int firstCurrentIndex, ResourcesConfig secConfig, int secondStartIndex = 0)
        {
	        bool res = true;
	        var iConfig = config.ListConfigs[firstCurrentIndex];
	        for (var j = secondStartIndex; j < secConfig.ListConfigs.Count; j++)
	        {
		        var jConfig = secConfig.ListConfigs[j];
		        if ((iConfig.Path.StartsWith(jConfig.Path) || jConfig.Path.StartsWith(iConfig.Path)) &&
		            (iConfig.Preset.GetTargetFullTypeName() == jConfig.Preset.GetTargetFullTypeName()))
		        {
			        // Same paths = additional checks need?
			        Debug.LogError($"Found conflict paths on config: {config}:\r\n" +
			                       $"{firstCurrentIndex}. {iConfig.Path}\r\n" +
			                       $"{j}. {jConfig.Path}", config);
			        if (config != secConfig)
			        {
						Debug.LogError($"\tSecond conflisct config is {secConfig}", secConfig);
			        }
			        res = false;
		        }
				// Can add any other checks
	        }

	        return res;
        }

        private static bool AdditionalCheckIsEqual(Preset infoPreset, Object resource)
        {
            var p = new Preset(resource);
            if (p.PropertyModifications is null) return false;
            foreach (PropertyModification pPropertyModification in p.PropertyModifications)
            {
                // For sprite importing we not need check this property because in uniq for sprites
                if (pPropertyModification.propertyPath.Contains("m_SpriteSheet.m_SpriteID")) continue;
                var presetModification = infoPreset.PropertyModifications.FirstOrDefault(pm =>
                    pm.propertyPath == pPropertyModification.propertyPath);
                if (presetModification == null) continue;
                if (string.Equals(presetModification.value, pPropertyModification.value,
                    StringComparison.OrdinalIgnoreCase)) continue;
                return false;
            }

            return true;
        }

        private static bool ApplyPresetToObj(Preset preset, Object resObj)
        {
            // base check
            if (preset.DataEquals(resObj)) return false;
            // Additional check with some extras 
            if (AdditionalCheckIsEqual(preset, resObj)) return false;

            return preset.ApplyTo(resObj);
        }
        

        private static void ApplyPreset(ResourcesInfo info, AssetImporter assetImporter, string newAssetPath = null)
        {
	        Object resObj = null;
	        try
	        {
		        resObj = AssetDatabase.LoadAssetAtPath(assetImporter.assetPath, typeof(Object));
	        }
	        catch (Exception exc)
	        {
				Debug.LogError(exc.Message + Environment.NewLine + exc.StackTrace);
	        }

	        string typeName = info.Preset.GetPresetType().GetManagedTypeName();
            // TODO: Cache FullTypeName => Type
            var targetType = GetTypeByName(typeName);
            if (typeof(Component).IsAssignableFrom(targetType))
            {
                var resGameObj = resObj as GameObject;
                if (resGameObj == null) return;

                var rootPrefabsList = new List<GameObject>();
                GetRootPrefabsList(resGameObj, rootPrefabsList);
                rootPrefabsList.Add(resGameObj);

                bool anyChanged = false;
                for (int i = 0; i < rootPrefabsList.Count; i++)
                {
                    bool isMainPrefabObj = i == rootPrefabsList.Count - 1;
                    GameObject prefabRoot = rootPrefabsList[i];
                    //Debug.Log($"{i}/{rootPrefabsList.Count}. Check " + prefabRoot + "; " + prefabRoot.GetHashCode(), prefabRoot);
                    var type = PrefabUtility.GetPrefabAssetType(prefabRoot);
                    switch (type)
                    {
                        case PrefabAssetType.Model:
                            // Can not apply as to prefab - so we need apply upper prefabRoot
                            continue;
                    }

                    var allComponents = prefabRoot.GetComponentsInChildren(targetType, true);
                    bool anyChangedOnPrefabRoot = false;
                    foreach (Component component in allComponents)
                    {
                        if (isMainPrefabObj)
                        {
                            var curPrefabRoot = PrefabUtility.GetNearestPrefabInstanceRoot(component);
                            if (curPrefabRoot != null)
                            {
                                continue;
                            }
                        }
                        if (rootPrefabsList.Contains(component.gameObject)) continue;
                        anyChangedOnPrefabRoot |= ApplyPresetToObj(info.Preset, component);
                        anyChanged |= anyChangedOnPrefabRoot;
                    }

                    switch (type)
                    {
                        case PrefabAssetType.Variant:
                            // TODO: 
                            break;
                        case PrefabAssetType.MissingAsset:
                            Debug.LogWarning("Found missing asset - " + prefabRoot, prefabRoot);
                            break;
                        case PrefabAssetType.Regular:
                            if (anyChangedOnPrefabRoot && resGameObj != prefabRoot)
                            {
	                            SavePrefabAsset(prefabRoot);
                            }

                            break;
                        default:
                        case PrefabAssetType.NotAPrefab:
                            throw new ArgumentOutOfRangeException();
                    }
                }

                if (anyChanged)
                {
                    SavePrefabAsset(resGameObj);
                }
                
                return;
            }

            if (info.Preset.CanBeAppliedTo(assetImporter))
            {
                if (ApplyPresetToObj(info.Preset, assetImporter))
                {
	                ForceImportAsset(newAssetPath ?? assetImporter.assetPath);
                }
                else
                {
	                //Debug.Log("[Reimport] Not need to apply preset assetImporter");
                }
                return;
            }

            if (info.Preset.CanBeAppliedTo(resObj))
            {
                if (ApplyPresetToObj(info.Preset, resObj))
                {
	                ForceImportAsset(newAssetPath ?? assetImporter.assetPath);
                }
                else
                {
	                //Debug.Log("[Reimport] Not need to apply preset Obj");
                }

                return;
            }
        }

        private static void SavePrefabAsset(GameObject resGameObj)
        {
	        try
	        {
		        //Debug.Log("[Reimport] SavePrefabAsset " + AssetDatabase.GetAssetPath(resGameObj) + "; " + resGameObj, resGameObj);
		        PrefabUtility.SavePrefabAsset(resGameObj);
		        s_changesCounter++;
	        }
	        catch (Exception exc)
	        {
		        Debug.LogError(exc.Message + Environment.NewLine + exc.StackTrace);
	        }
        }

        private static void ForceImportAsset(string assetPath)
        {
	        try
	        {
		        //Debug.Log("[Reimport] Force import " + assetPath);
		        AssetDatabase.ImportAsset(assetPath, ImportAssetOptions.ForceUpdate);
		        s_changesCounter++;
	        }
	        catch (Exception exc)
	        {
		        Debug.LogError(exc.Message + Environment.NewLine + exc.StackTrace);
	        }
        }

        private static void GetRootPrefabsList(GameObject resGameObj, List<GameObject> list)
        {
            foreach (Transform childTransform in resGameObj.transform)
            {
                GetRootPrefabsList(childTransform.gameObject, list);
            }
            var prefabRoot = PrefabUtility.IsAnyPrefabInstanceRoot(resGameObj);
            if (!prefabRoot)
            {
                return;
            }
            if (list.Contains(resGameObj))
            {
                return;
            }

            var path = PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(resGameObj);
            var obj = AssetDatabase.LoadAssetAtPath<GameObject>(path);
            list.Add(obj);
        }

        public static void FindAndReimport(string objPath)
        {
	        string newPath = null;
	        try
	        {
		        //Debug.Log("[MainFunc] Start Find and reimport " + objPath);
		        //Place the Asset Database in a state where
		        //importing is suspended for most APIs
		        AssetDatabase.StartAssetEditing();
		        FindResConfigs();
		        for (int i = 0; i < m_resConfigs.Count; i++)
		        {
			        var resConfig = m_resConfigs[i];
			        //Debug.Log($"{i}/{m_resConfigs.Count}. FindAndReimport " + resConfig + ": " + objPath);
			        foreach (ResourcesInfo info in resConfig.ListConfigs)
			        {
				        if (!objPath.StartsWith(info.Path)) continue;
				        //Debug.Log($"{i}/{m_resConfigs.Count}. {i2}/{resConfig.ListConfigs.Count}. FindAndReimport " + resConfig + ": " + objPath);
				        var assetImporter = AssetImporter.GetAtPath(objPath);
				        var asset = AssetDatabase.LoadAssetAtPath<Object>(objPath);
				        newPath = SetCorrectName.RenameFile(info.NameStyle, asset);
				        if (newPath != objPath)
				        {
					        s_changesCounter++;
				        }
				        ApplyPreset(info, assetImporter, newPath);
				        //ApplyPreset(info, assetImporter);
				        //Debug.Log(newPath + "; " + (newPath != objPath) + "\r\n[19:19:19] " + objPath);
			        }
		        }
	        }
	        catch (Exception exc)
	        {
		        Debug.LogError(exc.Message + Environment.NewLine + exc.StackTrace);
	        }
	        finally
	        {
		        //Debug.Log("[MainFunc] Stop " + objPath + "; \r\n" + newPath);
		        //By adding a call to StopAssetEditing inside
		        //a "finally" block, we ensure the AssetDatabase
		        //state will be reset when leaving this function
		        AssetDatabase.StopAssetEditing();
		        TryCallDelayedRefresh();
	        }
        }

        private static void TryCallDelayedRefresh()
        {
	        if (s_changesCounter > 0)
	        {
		        int curCounter = s_changesCounter;
		        EditorApplication.delayCall += () => CallRefresh(curCounter);
	        }
        }

        private static void CallRefresh(int curCounter)
        {
	        EditorApplication.delayCall += TryCallDelayedRefresh;
	        if (curCounter < s_changesCounter)
	        {
				return;
	        }
	        if (s_changesCounter <= 0)
	        {
				return;
	        }
			//Debug.Log("Refresh called for " + curCounter + " while " + s_changesCounter);
	        AssetDatabase.Refresh();
	        s_changesCounter -= curCounter;
        }

        public void ResourcesReimport(ResourcesInfo info)
        {
            string folderPath = Environment.CurrentDirectory + "/" + info.Path;
            if (!Directory.Exists(folderPath))
            {
                Debug.LogWarning("Directory not found " + info.Path);
                return;
            }
            var fileEntriesPaths = Directory.GetFiles(folderPath, "*.*", SearchOption.AllDirectories);
            var resources = new List<AssetImporter>();
            foreach (string fileEntriesPath in fileEntriesPaths)
            {
                string objectPath = fileEntriesPath.Replace(Environment.CurrentDirectory + "/", "");
                AssetImporter newObj = AssetImporter.GetAtPath(objectPath);
                if (newObj != null) resources.Add(newObj);
            }

            foreach (var resource in resources)
            {
	            var asset = AssetDatabase.LoadAssetAtPath<Object>(resource.assetPath);
	            var newPath = SetCorrectName.RenameFile(info.NameStyle, asset);
	            if (newPath != resource.assetPath)
	            {
		            s_changesCounter++;
	            }
	            ApplyPreset(info, resource, newPath);
                //ApplyPreset(info, resource);
            }
        }

        public void ReimportAll()
        {
            foreach (ResourcesInfo resourcesInfo in m_list)
            {
                ResourcesReimport(resourcesInfo);
            }
        }

    }
}