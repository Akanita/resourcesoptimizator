﻿using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace EditorUtils.ResOptimization
{
    [CustomEditor(typeof(ResourcesConfig))]
    public class ResourcesConfigEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            var list = serializedObject.FindProperty("m_list");
            Show(list, OnDrawElement);
            if (GUILayout.Button("Reimport all"))
            {
                var resConfig = target as ResourcesConfig;
                resConfig.ReimportAll();
            }

            serializedObject.ApplyModifiedProperties();
        }

        private static void Show(SerializedProperty list, Action<SerializedProperty> onDrawElement)
        {
            list.isExpanded = EditorGUILayout.Foldout(list.isExpanded, list.displayName);
            EditorGUI.indentLevel += 1;
            if (list.isExpanded)
            {
                EditorGUILayout.PropertyField(list.FindPropertyRelative("Array.size"));
                for (int i = 0; i < list.arraySize; i++)
                {
                    var element = list.GetArrayElementAtIndex(i);
                    EditorGUILayout.PropertyField(element);
                    onDrawElement(element);
                }
            }

            EditorGUI.indentLevel -= 1;
        }

        private void OnDrawElement(SerializedProperty element)
        {
            if (GUILayout.Button("Reimport"))
            {
                var resConfig = element.serializedObject.targetObject as ResourcesConfig;
                resConfig.ResourcesReimport(GetValue(element) as ResourcesConfig.ResourcesInfo);
            }
        }

        private static object GetValue(SerializedProperty prop)
        {
            if (prop == null) return null;

            var path = prop.propertyPath.Replace(".Array.data[", "[");
            object obj = prop.serializedObject.targetObject;
            var elements = path.Split('.');
            foreach (var element in elements)
            {
                if (element.Contains("["))
                {
                    var elementName = element.Substring(0, element.IndexOf("["));
                    var index = System.Convert.ToInt32(element.Substring(element.IndexOf("[")).Replace("[", "")
                        .Replace("]", ""));
                    obj = GetValue_Imp(obj, elementName, index);
                }
                else
                {
                    obj = GetValue_Imp(obj, element);
                }
            }

            return obj;
        }

        private static object GetValue_Imp(object source, string name, int index)
        {
            var enumerable = GetValue_Imp(source, name) as System.Collections.IEnumerable;
            if (enumerable == null) return null;
            var enm = enumerable.GetEnumerator();
            //while (index-- >= 0)
            //    enm.MoveNext();
            //return enm.Current;

            for (int i = 0; i <= index; i++)
            {
                if (!enm.MoveNext()) return null;
            }

            return enm.Current;
        }

        private static object GetValue_Imp(object source, string name)
        {
            if (source == null)
                return null;
            var type = source.GetType();

            while (type != null)
            {
                var f = type.GetField(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
                if (f != null)
                    return f.GetValue(source);

                var p = type.GetProperty(name,
                    BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
                if (p != null)
                    return p.GetValue(source, null);

                type = type.BaseType;
            }

            return null;
        }
    }
}